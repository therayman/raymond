# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)





 Appointment.create  (:specialist_id => 1, :patient_id => 1, complaint: 'Cancer', appointment_date: '2013-1-12 ', fee: '100.25')
 Appointment.create (:specialist_id => 2, :patient_id => 2 , complaint: 'Bordem', appointment_date: '2012-10-31 ', fee: '70.00')
 Appointment.create (:specialist_id =>3, :patient_id => 3, complaint: 'headache ', appointment_date: '2010-3-10 ', fee: '25.99')
 Appointment.create (:specialist_id => 4, :patient_id => 4 , complaint: 'stomach flu', appointment_date: '2014-12-25 ', fee: '67.45')
Appointment.create (:specialist_id  =>2, :patient_id => 5, complaint: 'Overdose', appointment_date: '2014-9-20 ', fee: '9.99')
Appointment.create (:specialist_id => 1, :patient_id => 1, complaint: 'low blood sugar', appointment_date: '2009-2-19 ', fee: '19.99')
Appointment.create (:specialist_id => 1, :patient_id =>1, complaint: 'lazyness', appointment_date: '2010-6-08 ', fee: '100.00')
Appointment.create (:specialist_id => 2, :patient_id => 2 , complaint: 'beiber fever', appointment_date: '2014-11-21 ', fee: '200.00')
 Appointment.create (:specialist_id => 4, :patient_id =>2, complaint: 'check up ', appointment_date: '2013-12-11 ', fee: '115.78')
Appointment.create (:specialist_id => 3, :patient_id  =>2, complaint: 'broken bone', appointment_date: '2014-7-4 ', fee: '79.98')