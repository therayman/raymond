class Patient < ActiveRecord::Base
  belongs_to :insurance
  has_many :specialists, :through => :appointments
  has_many :appointments

end
