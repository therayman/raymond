class Appointment < ActiveRecord::Base
  belongs_to :specialist
  belongs_to :patient
  validates_presence_of :fee
  validates(:fee, :numericality => {:greater_than_or_equal_to => 0, :message => 'Invalid Amount'})
end
